grpc-java (1.41.3+ds-5) unstable; urgency=medium

  * Drop ruby-* Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 20 Dec 2024 23:55:23 +0100

grpc-java (1.41.3+ds-4) unstable; urgency=medium

  * Team upload
  * Removing unneeded build-dependency on libopencensus-java (Closes: #979812)
  * Raising Standards version to 4.7.0 (no change)
  * Fixing clean rule (Closes: #1045096)

 -- Pierre Gruet <pgt@debian.org>  Wed, 08 May 2024 21:44:11 +0200

grpc-java (1.41.3+ds-3) unstable; urgency=medium

  * Team upload.
  * Specifying the classpaths of the built jars and using javahelper to define
    the list of dependencies (Closes: #1064215)
  * Removing unneeded versioned (build-)dependencies
  * Set upstream metadata fields: Security-Contact.

 -- Pierre Gruet <pgt@debian.org>  Sun, 18 Feb 2024 18:22:36 +0100

grpc-java (1.41.3+ds-2) unstable; urgency=high

  * Update dh_link directive in d/rules to prevent FTBFS (Closes: #1045144)
    - Thanks to Aurelien Jarno for the patch
  * Optimize d/rules with newer syntax

 -- Olek Wojnar <olek@debian.org>  Tue, 22 Aug 2023 19:05:13 -0400

grpc-java (1.41.3+ds-1) unstable; urgency=medium

  * New upstream version (Closes: #1021633)
    - Update patches
    - Add Build-Depend on libtomcat10-java
    - Add update-netty-utils.patch to fix incompatible types
  * Update standards to 4.6.2 (no changes)

 -- Olek Wojnar <olek@debian.org>  Tue, 24 Jan 2023 12:41:55 -0500

grpc-java (1.26.0+ds-2) unstable; urgency=high

  * Add patch to enable build with protobuf 3.21 (Closes: #1023500)
    - Thanks to László Böszörményi
  * Update d/watch for GitHub URL changes
  * Update standards to 4.6.1 (no changes)

 -- Olek Wojnar <olek@debian.org>  Wed, 23 Nov 2022 19:17:29 -0500

grpc-java (1.26.0+ds-1) unstable; urgency=high

  * New upstream version (Closes: #976487)
    - Refresh fix-gradle-build.patch
    - Refresh remove-android-annotations.patch
  * Update standards to 4.5.1 (no changes)
  * Add README.source with coordination request
    - For uploads of new upstream versions, to prevent interdependency issues

 -- Olek Wojnar <olek@debian.org>  Wed, 23 Dec 2020 16:59:31 -0500

grpc-java (1.20.0+ds-3) unstable; urgency=high

  * Remove Android dependency due to unavailability in Testing

 -- Olek Wojnar <olek@debian.org>  Mon, 02 Nov 2020 15:17:38 -0500

grpc-java (1.20.0+ds-2) unstable; urgency=medium

  * Add remaining modules required for Bazel

 -- Olek Wojnar <olek@debian.org>  Thu, 25 Jun 2020 00:56:18 -0400

grpc-java (1.20.0+ds-1) unstable; urgency=medium

  * Initial release (Closes: #959837)

 -- Olek Wojnar <olek@debian.org>  Sun, 19 Apr 2020 22:55:16 -0400
